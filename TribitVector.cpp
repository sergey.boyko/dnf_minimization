/**
* Created by Sergey O. Boyko on 20.11.18.
*/

#include "TribitVector.h"

// static implementation

TribitVector TribitVector::IncompleteGluing(const TribitVector &x,
																						const TribitVector &y) {
	CHECK(TribitVector::CanGlue(x, y), "couldn't glue vectors");

	TribitVector result(x);

	auto it_x = x.begin();
	auto it_y = y.begin();
	auto it_res = result.begin();

	while(it_x != x.end() && it_y != y.end()) {
		if(*it_x != *it_y) {
			it_res.SetValue('-');
		}

		++it_x;
		++it_y;
		++it_res;
	}

	return result;
}

bool TribitVector::CanGlue(const TribitVector &x, const TribitVector &y) {
	if(x.m_size != y.m_size) {
		return false;
	}

	auto it_x = x.begin();
	auto it_y = y.begin();

	int k = 0;
	while(it_x != x.end() && it_y != y.end()) {
		if(*it_x != *it_y) {
			if(++k > 1) {
				return false;
			}
		}

		++it_x;
		++it_y;
	}

	return true;
}

// TribitVector implementation

void TribitVector::init(std::string_view tribit_vector_by_str) {
	std::string unknown_bits_str;
	std::string bits_str;

	for(auto c : tribit_vector_by_str) {
		// '`' - special symbol which is equal to '0'
		CHECK(c == '0' || c == '1' || c == '-' || c == '~',
					"Incorrect input value");

		if(c == '-') {
			unknown_bits_str += '1';
			bits_str += '0';
		} else {
			unknown_bits_str += '0';
			bits_str += c;
		}
	}

	m_bits_vector = BitVector(bits_str);
	m_unknown_bits_vector = BitVector(unknown_bits_str);
	CHECK(m_bits_vector.Size() == m_unknown_bits_vector.Size(),
				"Incorrect result size: "
				"size of main bits vector = %, "
				"size of unknown bits vector = %");
	m_size = m_bits_vector.Size();
}

TribitVector::TribitVector(std::string_view tribit_vector_by_str) {
	init(tribit_vector_by_str);
}

TribitVector::TribitVector(TribitVector &&obj) noexcept:
		m_size(obj.m_size),
		m_bits_vector(std::move(obj.m_bits_vector)),
		m_unknown_bits_vector(std::move(obj.m_unknown_bits_vector)) {
}

TribitVector &TribitVector::operator=(TribitVector &&obj) noexcept {
	m_unknown_bits_vector = std::move(obj.m_unknown_bits_vector);
	m_bits_vector = std::move(obj.m_bits_vector);
	m_size = obj.m_size;
	return *this;
}

std::string TribitVector::ToString() const {
	std::string result;
	for(auto it : *this) {
		result.push_back(it);
	}

	return result;
}

TribitVector TribitVector::operator|(const TribitVector &obj) const {
	auto result = *this;
	result |= obj;

	return result;
}

TribitVector &TribitVector::operator|=(const TribitVector &obj) {
	auto obj_copy = obj;
	if(this->m_size < obj_copy.m_size) {
		this->IncreaseSize(obj_copy.m_size);
	} else if(this->m_size > obj_copy.m_size) {
		obj_copy.IncreaseSize(this->m_size);
	}

	this->m_bits_vector |= obj_copy.m_bits_vector;
	this->m_unknown_bits_vector |= obj_copy.m_unknown_bits_vector;

	auto it = m_bits_vector.begin();
	auto unknown_it = m_unknown_bits_vector.begin();

	while(it != m_bits_vector.end()
				&& unknown_it != m_bits_vector.end()) {
		if(*it == '1') {
			unknown_it.SetValue('0');
		}

		++it;
		++unknown_it;
	}

	return *this;
}

void TribitVector::IncreaseSize(std::size_t size) {
	m_size = size;
	m_bits_vector.IncreaseSize(m_size);
	m_unknown_bits_vector.IncreaseSize(m_size);
}

TribitVector &TribitVector::operator=(std::string_view tribit_vector_by_str) noexcept {
	init(tribit_vector_by_str);
	return *this;
}
