/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#pragma once


/**
* Remove duplicate lines starting from the current_line.
* This function must be called from the main loop
* @param table
* @param current_line
*/
inline void _RemoveDuplicateLines(PlaTable &table,
																	PlaTable::iterator current_line) {
	auto it = std::next(current_line);
	while(it != table.end()) {
		if((*it) == *current_line) {
			it = table.erase(it);
			continue;
		}

		++it;
	}
}

/**
* Get rid of uncertainty of current line:
* 1) find the next '-'
* 2) create copy of current_line
* 3) set '0' instead '-' in current_line1
*    and call itself with current_line
* 4) set '1' instead '-' in current_line2
*    and call itself with current_line = current_line2
* 5) if couldn't find '-' then push current_line to result_table
* @param result_table
* @param current_line1
* @param current_position
*/
inline void _GetRidOfUncertaintyRecursion(PlaTable &result_table,
																					PlaTableLine &current_line,
																					std::size_t current_position) {
	PlaTableLine &current_line1 = current_line;
	PlaTableLine current_line2(current_line);
	auto it1 = current_line1.m_input_vector.begin();
	auto it2 = current_line2.m_input_vector.begin();
	Advance(it1, current_position);
	Advance(it2, current_position);

	// *it1 always is the same *it2
	while(it1 != current_line1.m_input_vector.end() && *it1 != '-') {
		++it1;
		++it2;
		++current_position;
	}

	if(it1 != current_line1.m_input_vector.end()) {
		it1.SetValue('0');
		it2.SetValue('1');

		_GetRidOfUncertaintyRecursion(result_table,
																	current_line1,
																	current_position + 1);
		_GetRidOfUncertaintyRecursion(result_table,
																	current_line2,
																	current_position + 1);
	} else {
		result_table.push_back(current_line1);
	}
}

/**
* Get rid of uncertainty of current line
* @param current_line
* @return
*/
inline PlaTable
_GetRidOfUncertainty(PlaTable::iterator current_line) {
	PlaTable result;
	PlaTableLine line(current_line->m_input_vector, current_line->m_output_vector);
	_GetRidOfUncertaintyRecursion(result, line, 0);
	return result;
}

/**
* Redefine input dnf system by selecting one dnf
* '1' domination method is used
* @param root - input dnf system
* @param dnf_number - dnf selected number
* @return - redefined dnf (one dnf)
*/
inline PlaRoot _RedefineDnfSystem(const PlaRoot &root,
																	std::size_t dnf_number) {
	auto result_root = root;

	// remove all output variables except selected variable
	auto selected_it = result_root.m_info.m_output_variables.begin();
	std::advance(selected_it, dnf_number);
	result_root.m_info.m_output_variables
			= std::vector<std::string>(selected_it, std::next(selected_it));
	result_root.m_info.m_output_variables_count = 1;

	// remove all lines where selected output variable is 0
	auto it = result_root.m_table.begin();
	while(it != result_root.m_table.end()) {
		if(*GetPosition<TribitVector::Iterator>
				(it->m_output_vector, dnf_number)
			 == '0') {
			it = result_root.m_table.erase(it);
			continue;
		}

		// replace output bit vector to '1'
		it->m_output_vector = "1";

		_RemoveDuplicateLines(result_root.m_table, it);

		auto after_removing_uncertaintly = _GetRidOfUncertainty(it);
		if(!after_removing_uncertaintly.empty()) {
			//it = result_root.m_table.erase(it);
			auto next_it = result_root.m_table.insert(result_root.m_table.end(),
																								after_removing_uncertaintly.begin(),
																								after_removing_uncertaintly.end());
			it = next_it - 1;
			it = result_root.m_table.erase(it);
			std::advance(it, after_removing_uncertaintly.size());
		} else {
			++it;
		}

	}

	result_root.m_info.m_pla_table_size = result_root.m_table.size();
	return result_root;
}