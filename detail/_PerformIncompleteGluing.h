/**
* Created by Sergey O. Boyko on 19.12.18.
*/


#pragma once


#include "dnf_system_minimization/_GroupedPlaRoot.h"
#include "CalculateLineWeight.h"

inline bool _IncompleteGluingBetweenGroups(_GroupedPlaRoot &root,
																					 _GroupedPlaRoot::_Group &x,
																					 _GroupedPlaRoot::_Group &y) {
	bool was_gluing = false;
	for(auto &it_x : x) {
		for(auto &it_y : y) {
			if(TribitVector::CanGlue(it_x.m_input_vector,
															 it_y.m_input_vector)) {
				it_x.m_is_used = true;
				it_y.m_is_used = true;

				was_gluing = true;
				auto result_line =
						TribitVector::IncompleteGluing(it_x.m_input_vector,
																					 it_y.m_input_vector);

				auto weight = CalculateLineWeight(result_line);
				root.m_grouped_tables[weight].emplace_back(result_line);
			}

		}
	}

	return was_gluing;
}

inline std::optional<_GroupedPlaRoot>
_PerformIncompleteGluingBetweenGroups(_GroupedPlaRoot &root) {
	bool was_gluing = false;
	_GroupedPlaRoot result;

	auto it_group = root.m_grouped_tables.begin();
	while(it_group != root.m_grouped_tables.end()) {
		auto it_next_group = std::next(it_group);
		if(it_next_group == root.m_grouped_tables.end()) {
			break;
		}

		was_gluing =
				_IncompleteGluingBetweenGroups(result,
																			 it_group->second,
																			 it_next_group->second)
				? true : was_gluing;

		++it_group;
	}

	if(was_gluing) {
		return result;
	}

	return {};
};

