/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#pragma once

#include "_GroupedPlaRoot.h"
#include "../CalculateLineWeight.h"

inline _GroupedPlaRoot _GroupByWeight(const PlaRoot &root) {
	_GroupedPlaRoot result_root;
	for(auto it = root.m_table.begin(); it != root.m_table.end(); ++it) {
		auto weight = CalculateLineWeight(it->m_input_vector);
		result_root.m_grouped_tables[weight].emplace_back(it);
	}

	return result_root;
}