/**
* Created by Sergey O. Boyko on 21.12.18.
*/

#pragma once

#include "../../PlaRoot.h"
#include "_GroupedPlaRoot.h"

inline PlaRoot _GenerateMinimizedPlaRoot(const PlaInfo &root_info, const std::list<_GroupedPlaRoot> &gluing_steps) {
	PlaRoot result;

	result.m_info = root_info;

	for(const auto &steps : gluing_steps) {
		for(const auto &group : steps.m_grouped_tables) {
			for(const auto &line : group.second) {
				if(!line.m_is_used) {
					result.m_table.emplace_back(line.m_input_vector, TribitVector("1"));
				}
			}
		}
	}

	result.m_info.m_pla_table_size = result.m_table.size();
	return result;
}