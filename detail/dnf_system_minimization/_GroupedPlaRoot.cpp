/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#include "_GroupedPlaRoot.h"

void _RemoveNextDuplicateLines(_GroupedPlaRoot::_Group &group,
															 _GroupedPlaRoot::_Group::iterator current_line) {
	auto it = std::next(current_line);
	while(it != group.end()) {
		if((*it) == *current_line) {
			it = group.erase(it);
			continue;
		}

		++it;
	}
}

void _GroupedPlaRoot::RemoveDuplicateLines() {
	auto it_map = m_grouped_tables.begin();
	while(it_map != m_grouped_tables.end()) {
		auto it_line = it_map->second.begin();
		while(it_line != it_map->second.end()) {
			_RemoveNextDuplicateLines(it_map->second, it_line);

			++it_line;
		}

		if(it_map->second.empty()) {
			it_map = m_grouped_tables.erase(it_map);
			continue;
		}

		++it_map;
	}
}
