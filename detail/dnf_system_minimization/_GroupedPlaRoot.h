/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#pragma once

#include <list>
#include <map>

#include "../../PlaRoot.h"

struct GroupLineWrapper {
	GroupLineWrapper() = default;

	explicit GroupLineWrapper(PlaTable::const_iterator it):
			m_input_vector(it->m_input_vector),
			m_table_it(it) {
	}

	GroupLineWrapper(const GroupLineWrapper &obj) = default;

	explicit GroupLineWrapper(const TribitVector &new_input_vector):
			m_input_vector(new_input_vector) {
	}

	inline bool operator==(const GroupLineWrapper &obj) {
		return m_input_vector == obj.m_input_vector;
	}

	/**
	* Appropriate PlaTable iterator
	*/
	PlaTable::const_iterator m_table_it;

	TribitVector m_input_vector;

	/**
	* True if the line participated in gluing of vectors
	*/
	bool m_is_used = false;
};

struct _GroupedPlaRoot {
	void RemoveDuplicateLines();

	inline std::string ToString() {
		std::stringstream stream;
		for(const auto &it : m_grouped_tables) {
			stream << core::base::Format("group [weight = %]:", it.first) << std::endl;
			for(const auto &it_line : it.second) {
				stream << it_line.m_input_vector.ToString() << std::endl;
			}
		}

		return stream.str();
	}

	typedef std::list<GroupLineWrapper> _Group;

	std::map<std::size_t, _Group> m_grouped_tables;
};

