/**
* Created by Sergey O. Boyko on 19.12.18.
*/

#pragma once

#include "../TribitVector.h"

template <class T>
std::size_t CalculateLineWeight(const T &vector) {
	std::size_t weight = 0;
	for(auto it : vector) {
		if(it == '1') {
			++weight;
		}
	}

	return weight;
}