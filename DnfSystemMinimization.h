/**
* Created by Sergey O. Boyko on 05.12.18.
*/

#pragma once

#include "ParsePla.h"

std::optional<PlaRoot> MinimizeDnfSystemByQuineMcCluskey(const PlaRoot &root,
																												 std::size_t dnf_number);