/**
* Created by Sergey O. Boyko on 05.12.18.
*/

#include "DnfSystemMinimization.h"

#include "detail/_RedefineDnfSystem.h"
#include "detail/dnf_system_minimization/_GroupByWeights.h"
#include "detail/_PerformIncompleteGluing.h"
#include "detail/dnf_system_minimization/_GenerateMinimizedPlaRoot.h"

std::optional<PlaRoot> MinimizeDnfSystemByQuineMcCluskey(const PlaRoot &root,
																												 std::size_t dnf_number) {
	//TODO check pla_info
	// TODO check size of output vector and dnf_number
	auto redefined_root = _RedefineDnfSystem(root, dnf_number);

	LOG_D("PlaRoot after redefining:\n%", redefined_root.ToString());

	std::list<_GroupedPlaRoot> gluing_steps;

	gluing_steps.push_back(_GroupByWeight(redefined_root));

	while(auto gluing_result
			= _PerformIncompleteGluingBetweenGroups(*gluing_steps.rbegin())) {
		gluing_result->RemoveDuplicateLines();
		gluing_steps.push_back(*gluing_result);
		LOG_D("Next step:\n%", gluing_result->ToString());
	}

	return _GenerateMinimizedPlaRoot(redefined_root.m_info, gluing_steps);
}