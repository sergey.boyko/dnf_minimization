/**
* Created by Sergey O. Boyko on 24.11.18.
*/

#pragma once

#include <cstddef>

template <class IteratorT>
class BaseIterator {
public:
	virtual IteratorT operator++() = 0;

	virtual const IteratorT operator++(int) {
		IteratorT copy(*static_cast<IteratorT *>(this));
		operator++();

		return copy;
	};
};

template <class IteratorT>
void Advance(IteratorT &&it, std::size_t distance) {
	for(auto i = 0; i < distance; ++i) {
		++it;
	}
}

template <class IteratorT, class ValueT>
IteratorT GetPosition(ValueT &&value, std::size_t position) {
	IteratorT it = value.begin();
	Advance(it, position);
	return it;
}