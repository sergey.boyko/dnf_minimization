/**
* Created by Sergey O. Boyko on 11.11.18.
*/

#include <base/Logger.h>
#include <iterator>
#include "ParsePla.h"

namespace PlaCommand {
const std::string Input = ".i";
const std::string Output = ".o";
const std::string InputVariables = ".ilb";
const std::string OutputVariables = ".ob";
const std::string NumberOfLines = ".p";
const std::string EndOfFile = ".e";
}

std::vector<std::string> splitLine(const std::string &line) {
	std::istringstream line_stream(line);
	std::string word;

	std::vector<std::string> splitted_line(
			std::istream_iterator<std::string> {line_stream},
			std::istream_iterator<std::string>());
	return splitted_line;
}

/**
* Process input command (number of input variables)
* Can throw exception
* @param splitted_line
* @param result
* @return true if command is parsed correctly
*/
bool processCountCommand(const std::vector<std::string> &splitted_line,
												 std::size_t &result) {
	if(splitted_line.size() != 2) {
		return false;
	}

	result = static_cast<size_t>(std::stoul(splitted_line[1]));
	return true;
}

/**
* Process input variables command (list of input variables)
* @param splitted_line
* @param result
* @return true if command is parsed correctly
*/
bool processVariablesCommand(const std::vector<std::string> &splitted_line,
														 std::vector<std::string> &result) {
	if(splitted_line.size() < 2) {
		return false;
	}

	// run loop with second word
	for(auto it = splitted_line.begin() + 1; it != splitted_line.end(); ++it) {
		result.push_back(*it);
	}

	return true;
}

bool processPlaInfo(PlaInfo &result, std::ifstream &input_file_stream) {
	std::string line;
	while(input_file_stream && !result.IsFilled()) {
		std::getline(input_file_stream, line);
		auto words = splitLine(line);

		if(line.empty()) {
			continue;
		}

		bool is_parsed_correctly = false;
		try {
			if(*words.begin() == PlaCommand::Input) {
				is_parsed_correctly = processCountCommand(words, result.m_input_variables_count);

			} else if(*words.begin() == PlaCommand::Output) {
				is_parsed_correctly = processCountCommand(words, result.m_output_variables_count);

			} else if(*words.begin() == PlaCommand::InputVariables) {
				is_parsed_correctly = processVariablesCommand(words, result.m_input_variables);

			} else if(*words.begin() == PlaCommand::OutputVariables) {
				is_parsed_correctly = processVariablesCommand(words, result.m_output_variables);

			} else if(*words.begin() == PlaCommand::NumberOfLines) {
				is_parsed_correctly = processCountCommand(words, result.m_pla_table_size);
			}

			if(!is_parsed_correctly) {
				LOG_E("Incorrect format. Couldn't parse line: '%'", line);
				return false;
			}
		} catch(const std::invalid_argument &ex) {
			LOG_E("Couldn't parse '%' command: %",
						*words.begin(),
						ex.what());
			return false;
		}
	}

	LOG_D("Pla header has been parsed. Result = %", result.IsFilled());
	return result.IsFilled();
}

bool processPlaTable(PlaTable &result, std::ifstream &input_file_stream) {
	std::string line;
	while(input_file_stream) {
		std::getline(input_file_stream, line);
		if(line.empty()) {
			continue;
		}

		auto words = splitLine(line);
		if(*words.begin() == PlaCommand::EndOfFile) {
			return true;
		}

		if(words.size() != 2) {
			LOG_E("Incorrect table line: line must contain two bit vectors, but contain %",
						words.size());
			return false;
		}

		result.emplace_back(words[0], words[1]);
	}

	LOG_E("File must end with the '%' command", PlaCommand::EndOfFile);
	return false;
};

std::optional<PlaRoot> ParsePla(const std::string &filename) {
	std::ifstream pla_stream(filename);
	if(!pla_stream) {
		LOG_E("Couldn't open the file");
		return {};
	}

	PlaRoot root;
	if(!processPlaInfo(root.m_info, pla_stream)) {
		return {};
	}

	if(!processPlaTable(root.m_table, pla_stream)) {
		return {};
	}

	return root;
}