/**
* Created by Sergey O. Boyko on 20.11.18.
*/

#pragma once

#include "BitVector.h"

class TribitVector {
public:
	static TribitVector IncompleteGluing(const TribitVector &x, const TribitVector &y);

	static bool CanGlue(const TribitVector &x, const TribitVector &y);

	TribitVector() = default;

	~TribitVector() = default;

	explicit TribitVector(std::string_view tribit_vector_by_str);

	TribitVector(const TribitVector &obj) = default;

	TribitVector(TribitVector &&obj) noexcept;

	TribitVector &operator=(const TribitVector &obj) = default;

	TribitVector &operator=(TribitVector &&obj) noexcept;

	TribitVector &operator=(std::string_view tribit_vector_by_str) noexcept;

	TribitVector operator|(const TribitVector &obj) const;

	TribitVector &operator|=(const TribitVector &obj);

	bool operator==(const TribitVector &obj) const {
		return m_size == obj.m_size
					 && m_bits_vector == obj.m_bits_vector
					 && m_unknown_bits_vector == obj.m_unknown_bits_vector;
	}

	bool operator!=(const TribitVector &obj) const {
		return !(*this == obj);
	}

	std::string ToString() const;

	inline std::size_t Size() {
		return m_size;
	}

	void IncreaseSize(std::size_t size);

	class IConstIteratorValue {
	public:
		explicit IConstIteratorValue(const TribitVector *value):
				m_data(value) {
		}

		IConstIteratorValue() = default;

		IConstIteratorValue &operator=(const IConstIteratorValue &obj) = default;

		IConstIteratorValue &operator=(const TribitVector *value) {
			m_data = value;

			return *this;
		}

		const TribitVector *operator->() {
			return m_data;
		}

		const TribitVector *m_data = nullptr;
	}; // end of IConstIteratorValue class

	class INonConstIteratorValue {
	public:
		explicit INonConstIteratorValue(TribitVector *value):
				m_data(value) {
		}

		INonConstIteratorValue() = default;

		INonConstIteratorValue &operator=(const INonConstIteratorValue &obj) = default;

		INonConstIteratorValue &operator=(TribitVector *value) {
			m_data = value;

			return *this;
		}

		TribitVector *operator->() {
			return m_data;
		}

		TribitVector *m_data = nullptr;
	}; // end of INonConstIteratorValue class

	template <class IteratorValueT, class IteratorT, class UsedBitIteratorT>
	class BaseIteratorImpl: public BaseIterator<IteratorT> {
	public:
		inline explicit operator bool() const {
			return m_value.m_data != nullptr;
		}

		inline bool operator==(const BaseIteratorImpl &it) const {
			return m_value.m_data == it.m_value.m_data
						 && m_bits_iterator == it.m_bits_iterator
						 && m_unknown_bits_iterator == it.m_unknown_bits_iterator;
		}

		inline bool operator!=(const BaseIteratorImpl &it) const {
			return !(*this == it);
		}

		virtual char operator*() const {
			CHECK_D(m_value.m_data, "Value is nullptr");
			if(*m_unknown_bits_iterator == '1') {
				return '-';
			}

			return *m_bits_iterator;
		}

		IteratorT operator++() override {
			CHECK_D(m_value.m_data, "Value is nullptr");

			++m_bits_iterator;
			++m_unknown_bits_iterator;

			if(!m_bits_iterator || !m_unknown_bits_iterator) {
				m_value = nullptr;
			}

			return *static_cast<IteratorT *>(this);
		}

		BaseIteratorImpl() = default;

		BaseIteratorImpl(const BaseIteratorImpl &) = default;

	protected:
		IteratorValueT m_value;

		UsedBitIteratorT m_bits_iterator;
		UsedBitIteratorT m_unknown_bits_iterator;
	}; // end of BaseIteratorImpl class

	template <class IteratorT, class UsedBitIteratorT>
	class IConstIteratorImpl:
			public BaseIteratorImpl<IConstIteratorValue, IteratorT, UsedBitIteratorT> {
	public:
		explicit IConstIteratorImpl(const TribitVector *value) {
			Base::m_value = value;
			Base::m_bits_iterator = UsedBitIteratorT(&value->m_bits_vector);
			Base::m_unknown_bits_iterator = UsedBitIteratorT(&value->m_unknown_bits_vector);
		}

	protected:
		typedef BaseIteratorImpl<IConstIteratorValue, IteratorT, UsedBitIteratorT> Base;

		IConstIteratorImpl() = default;

		IConstIteratorImpl(const IConstIteratorImpl &) = default;
	}; // end of IConstIteratorImpl class

	template <class IteratorT, class UsedBitIteratorT>
	class INonConstIteratorImpl:
			public BaseIteratorImpl<INonConstIteratorValue, IteratorT, UsedBitIteratorT> {
	public:
		explicit INonConstIteratorImpl(TribitVector *value) {
			Base::m_value = value;
			Base::m_bits_iterator = UsedBitIteratorT(&value->m_bits_vector);
			Base::m_unknown_bits_iterator = UsedBitIteratorT(&value->m_unknown_bits_vector);
		}

		inline void SetValue(char value) {
			CHECK(value == '0' || value == '1' || value == '-', "Incorrect value");

			if(value == '-') {
				Base::m_unknown_bits_iterator.SetValue('1');
				Base::m_bits_iterator.SetValue('0');
			} else {
				Base::m_unknown_bits_iterator.SetValue('0');
				Base::m_bits_iterator.SetValue(value);
			}
		}

	protected:
		typedef BaseIteratorImpl<INonConstIteratorValue, IteratorT, UsedBitIteratorT> Base;

		INonConstIteratorImpl() = default;

		INonConstIteratorImpl(const INonConstIteratorImpl &) = default;
	}; // end of INonConstIteratorImpl class

	class Iterator: public INonConstIteratorImpl<Iterator, BitVector::Iterator> {
	public:
		explicit Iterator(TribitVector *value):
				INonConstIteratorImpl(value) {
		}

		Iterator() = default;

		Iterator(const Iterator &) = default;
	}; // end of Iterator class

	class ConstIterator:
			public IConstIteratorImpl<ConstIterator, BitVector::ConstIterator> {
	public:
		explicit ConstIterator(const TribitVector *value):
				IConstIteratorImpl(value) {
		}

		ConstIterator() = default;

		ConstIterator(const ConstIterator &) = default;
	}; // end of ConstIterator class

	class ReverseIterator:
			public INonConstIteratorImpl<ReverseIterator, BitVector::ReverseIterator> {
	public:
		explicit ReverseIterator(TribitVector *value):
				INonConstIteratorImpl(value) {
		}

		ReverseIterator() = default;

		ReverseIterator(const ReverseIterator &) = default;
	};

	class ConstReverseIterator:
			public IConstIteratorImpl<ConstIterator, BitVector::ConstIterator> {
	public:
		explicit ConstReverseIterator(const TribitVector *value):
				IConstIteratorImpl(value) {
		}

		ConstReverseIterator() = default;

		ConstReverseIterator(const ConstReverseIterator &) = default;
	}; // end of ConstReverseIterator class

	inline Iterator begin() {
		return Iterator(this);
	}

	inline ConstIterator begin() const {
		return ConstIterator(this);
	}

	inline Iterator end() {
		return Iterator();
	}

	inline ConstIterator end() const {
		return ConstIterator();
	}

	inline ReverseIterator rbegin() {
		return ReverseIterator(this);
	}

	inline ConstReverseIterator rbegin() const {
		return ConstReverseIterator(this);
	}

	inline ReverseIterator rend() {
		return ReverseIterator();
	}

	inline ConstReverseIterator rend() const {
		return ConstReverseIterator();
	}

protected:
	void init(std::string_view tribit_vector_by_str);

	std::size_t m_size = 0;
	BitVector m_bits_vector;
	BitVector m_unknown_bits_vector;
};

