/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#include "PlaRoot.h"

PlaTableLine::PlaTableLine(std::string_view input_line_str,
													 std::string_view output_line_str):
		m_input_vector(input_line_str),
		m_output_vector(output_line_str) {
}

bool PlaTableLine::operator==(const PlaTableLine &obj) {
	return m_input_vector == obj.m_input_vector
				 && m_output_vector == obj.m_output_vector;
}

PlaTableLine::PlaTableLine(const TribitVector &input_line,
													 const TribitVector &output_line):
		m_input_vector(input_line),
		m_output_vector(output_line) {
}

std::string PlaInfo::ToString() {
	std::string result;
	result += core::base::Format("Size of input variables: %\n",
															 m_input_variables_count);
	result += core::base::Format("Size of output variables: %\n",
															 m_output_variables_count);

	std::string input_variables;
	for(const auto &it : m_input_variables) {
		input_variables += it + ", ";
	}
	result += core::base::Format("Input variables: %\n", input_variables);

	std::string output_variables;
	for(const auto &it : m_output_variables) {
		output_variables += it + ", ";
	}
	result += core::base::Format("Output variables: %\n", output_variables);

	result += core::base::Format("Size of Pla table: %\n",
															 m_pla_table_size);

	return result;
}


std::string PlaRoot::ToString() {
	auto result = m_info.ToString();

	for(auto &it : m_table) {
		result += core::base::Format("input: %, output: %\n",
																 it.m_input_vector.ToString(),
																 it.m_output_vector.ToString());
	}

	return result;
}
