/**
* Created by Sergey O. Boyko on 10.11.18.
*/

#include "BitVector.h"

BitVector::BitVector(BitVector &&obj) noexcept:
		m_bytes(std::move(obj.m_bytes)),
		m_size(obj.m_size),
		m_real_byte_count(obj.m_real_byte_count),
		m_higher_bit_position_in_last_byte(obj.m_higher_bit_position_in_last_byte) {
}


BitVector &BitVector::operator=(BitVector &&obj) noexcept {
	m_bytes = std::move(obj.m_bytes);
	m_size = obj.m_size;
	m_real_byte_count = obj.m_real_byte_count;
	m_higher_bit_position_in_last_byte = obj.m_higher_bit_position_in_last_byte;
	return *this;
}


BitVector::BitVector(std::string_view bit_vector_by_str) {
	init(bit_vector_by_str);
}


std::string BitVector::ToString() const {
	std::string result;
	for(auto it : *this) {
		result.push_back(it);
	}

	return result;
}


void BitVector::init(std::string_view bit_vector_by_str) {
	//TODO remove it
	CHECK(!bit_vector_by_str.empty(), "Incorrect input bit vector");
	m_size = bit_vector_by_str.size();

	m_real_byte_count = calculateRealByteCount(m_size);
	// fill m_bytes with 0
	m_bytes.assign(m_real_byte_count, 0);

	// set highest bit of last byte
	// for example: 11010.10111010, higher bit position = 4
	m_higher_bit_position_in_last_byte = calculateHigherBitPositionInLastByte(m_size);

	auto it = this->begin();

	// use long i because std::size_t cannot be < 0
	for(auto input_symbol : bit_vector_by_str) {
		it.SetValue(input_symbol);
		++it;
	}
}

void BitVector::IncreaseSize(std::size_t size) {
	CHECK_D(m_size < size, "Incorrect input size");

	m_size = size;
	m_higher_bit_position_in_last_byte = calculateHigherBitPositionInLastByte(m_size);
	auto new_real_byte_count = calculateRealByteCount(size);

	for(auto i = m_real_byte_count; i < new_real_byte_count; ++i) {
		m_bytes.push_back(0);
	}

	m_real_byte_count = new_real_byte_count;
}

BitVector BitVector::operator|(const BitVector &obj) const {
	BitVector result = *this;
	result |= obj;

	return result;
}

BitVector &BitVector::operator|=(const BitVector &obj) {
	auto obj_copy = obj;

	if(this->m_size < obj_copy.m_size) {
		this->IncreaseSize(obj_copy.m_size);
	} else if(this->m_size > obj_copy.m_size) {
		obj_copy.IncreaseSize(this->m_size);
	}

	auto res_it = this->begin();
	auto obj_it = obj_copy.begin();
	for(; res_it != this->end() && obj_it != obj_copy.end();
				++res_it, ++obj_it) {
		if(*res_it == '0' && *obj_it == '1') {
			res_it.SetValue('1');
		}
	}

	return *this;
}


BitVector &BitVector::operator=(std::string_view bit_vector_by_str) {
	init(bit_vector_by_str);

	return *this;
}

/**
* BitVector iterator
*/

template <class IteratorValueT, class IteratorT>
void BitVector::BaseIteratorImpl<IteratorValueT, IteratorT>::stepUp() {
	CHECK_D(m_value.m_data, "Value is nullptr");

	--m_current_bit_position;

	if(m_current_bit_position < 0) {
		--m_current_byte_position;

		m_current_bit_position = BITS_PER_BYTE - 1;
	}

	if(m_current_byte_position < 0) {
		m_value = nullptr;
		m_current_byte_position = 0;
		m_current_bit_position = 0;
	}
}

template <class IteratorValueT, class IteratorT>
void BitVector::BaseIteratorImpl<IteratorValueT, IteratorT>::stepDown() {
	CHECK_D(m_value.m_data, "Value is nullptr");

	++m_current_bit_position;

	if(m_current_bit_position >= BITS_PER_BYTE) {
		++m_current_byte_position;

		m_current_bit_position = 0;
	}

	auto real_byte_count = m_value->m_real_byte_count;
	auto higher_bit_position = m_value->m_higher_bit_position_in_last_byte;

	if(m_current_byte_position >= real_byte_count
		 || (m_current_byte_position == real_byte_count - 1
				 && m_current_bit_position > higher_bit_position)) {
		m_value = nullptr;
		m_current_byte_position = 0;
		m_current_bit_position = 0;
	}
}
