/**
* Created by Sergey O. Boyko on 13.12.18.
*/

#pragma once

#include "TribitVector.h"

/**
* PlaInfo - std::map where key is Pla command, value is Pla input line
* Pla file example:
* .i 2
* .o 1
* .ilb a b
* .ob output
* .p 3
* 01000 1
* 10000 1
* 00001 1
* .e
*
* PlaInfo usage:
* PlaInputLine line = pla_info[INPUT_VARIABLES];
*/
struct PlaInfo {
	PlaInfo() = default;

	PlaInfo(const PlaInfo &) = default;

	std::size_t m_input_variables_count = 0;
	std::size_t m_output_variables_count = 0;
	std::vector<std::string> m_input_variables;
	std::vector<std::string> m_output_variables;
	std::size_t m_pla_table_size = 0;

	inline bool IsFilled() {
		return m_input_variables_count
					 && m_output_variables_count
					 && !m_input_variables.empty()
					 && !m_output_variables.empty()
					 && m_pla_table_size;
	}

	std::string ToString();
};

struct PlaTableLine {
	PlaTableLine() = default;

	explicit PlaTableLine(std::string_view input_line_str,
												std::string_view output_line_str);

	explicit PlaTableLine(const TribitVector &input_line,
												const TribitVector &output_line);

	PlaTableLine(const PlaTableLine &obj) = default;

	bool operator==(const PlaTableLine &obj);

	TribitVector m_input_vector;
	TribitVector m_output_vector;
};

typedef std::vector<PlaTableLine> PlaTable;

struct PlaRoot {
	PlaInfo m_info;
	PlaTable m_table;

	std::string ToString();
};