/**
* Created by Sergey O. Boyko on 09.11.18.
*/

#include "base/Logger.h"

#include "BitVector.h"
#include "ParsePla.h"
#include "TribitVector.h"
#include "DnfSystemMinimization.h"

void PrintHelp() noexcept {
	std::cout << std::endl << "--help (show program options)"
						<< std::endl << "filename (path to PLA file)"
						<< std::endl;
}

int main(int argc, char *argv[]) {
	Logger::UseConsoleOutput();
	Logger::SetModuleName("dnf_minimization");
	if(!Logger::InitLogger()) {
		std::cerr << Logger::GetLastError();
		return 1;
	}

	if(argc != 2) {
		LOG_E("Incorrect program options");
		PrintHelp();
		return 1;
	}

	std::string program_option(argv[1]);
	if(program_option == "--help") {
		PrintHelp();
		return 0;
	}

	LOG_I("Enter dnf number");
	int dnf_number;
	std::cin >> dnf_number;

	if(dnf_number < 0) {
		LOG_E("Incorrect dnf number (%)", dnf_number);
		return 1;
	}

	auto pla_root = ParsePla(program_option);
	LOG_I("Input Pla file:\n%", (*pla_root).ToString());
	auto minimized_pla_root =
			MinimizeDnfSystemByQuineMcCluskey(*pla_root, static_cast<size_t>(dnf_number));

	LOG_I("Minimized Pla:\n%", minimized_pla_root->ToString());
}