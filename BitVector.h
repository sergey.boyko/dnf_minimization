/**
* Created by Sergey O. Boyko on 10.11.18.
*/

#pragma once

#include <vector>
#include <cstdint>
#include <string_view>
#include <base/Checking.h>

#include "BaseIterator.h"

//: public BaseValueRange<BitVector, Iterator, ReverseIterator>

class BitVector {
public:
	BitVector() = default;

	~BitVector() = default;

	explicit BitVector(std::string_view bit_vector_by_str);

	BitVector(const BitVector &obj) = default;

	BitVector(BitVector &&obj) noexcept;

	BitVector &operator=(const BitVector &obj) = default;

	BitVector &operator=(BitVector &&obj) noexcept;

	BitVector &operator=(std::string_view bit_vector_by_str);

	BitVector operator|(const BitVector &obj) const;

	BitVector &operator|=(const BitVector &obj);

	bool operator==(const BitVector &obj) const {
		return m_bytes == obj.m_bytes
					 && m_size == obj.m_size;
	}

	bool operator!=(const BitVector &obj) const {
		return !(*this == obj);
	}

	inline explicit operator bool() const {
		return m_size != 0;
	}

	std::string ToString() const;

	inline std::size_t Size() const {
		return m_size;
	}

	void IncreaseSize(std::size_t size);

	class IConstIteratorValue {
	public:
		explicit IConstIteratorValue(const BitVector *value):
				m_data(value) {
		}

		IConstIteratorValue() = default;

		IConstIteratorValue &operator=(const IConstIteratorValue &obj) = default;

		IConstIteratorValue &operator=(const BitVector *value) {
			m_data = value;

			return *this;
		}

		const BitVector *operator->() const {
			return m_data;
		}

		const BitVector *m_data = nullptr;
	}; // end of IConstIteratorValue class

	class INonConstIteratorValue {
	public:
		explicit INonConstIteratorValue(BitVector *value):
				m_data(value) {
		}

		INonConstIteratorValue() = default;

		INonConstIteratorValue &operator=(const INonConstIteratorValue &obj) = default;

		INonConstIteratorValue &operator=(BitVector *value) {
			m_data = value;

			return *this;
		}

		BitVector *operator->() {
			return m_data;
		}

		BitVector *m_data = nullptr;
	}; // end of INonConstIteratorValue class

	template <class IteratorValueT, class IteratorT>
	class BaseIteratorImpl: public BaseIterator<IteratorT> {
	public:
		inline explicit operator bool() {
			return m_value.m_data != nullptr;
		}

		inline bool operator==(const BaseIteratorImpl &it) const {
			return m_value.m_data == it.m_value.m_data
						 && m_current_byte_position == it.m_current_byte_position
						 && m_current_bit_position == it.m_current_bit_position;
		}

		inline bool operator!=(const BaseIteratorImpl &it) const {
			return !(*this == it);
		}

		inline char operator*() const {
			CHECK_D(m_value.m_data, "Value is nullptr");

			uint8_t mask = 1;
			mask <<= m_current_bit_position;

			if(m_value.m_data->m_bytes[m_current_byte_position] & mask) {
				return '1';
			}

			return '0';
		}

	protected:
		friend class BitVector;

		BaseIteratorImpl() = default;

		BaseIteratorImpl(const BaseIteratorImpl &) = default;

		void initHighPosition() {
			m_current_byte_position = m_value->m_real_byte_count - 1;
			m_current_bit_position =
					static_cast<uint8_t>(m_value->m_higher_bit_position_in_last_byte);
		}

		void initLowPosition() {
			m_current_byte_position = 0;
			m_current_bit_position = 0;
		}

		void stepUp();

		void stepDown();

		IteratorValueT m_value;

		long m_current_byte_position = 0;
		long m_current_bit_position = 0;
	}; // end of BaseIteratorImpl class

	template <class IteratorT>
	class IConstIteratorImpl: public BaseIteratorImpl<IConstIteratorValue, IteratorT> {
	public:
		explicit IConstIteratorImpl(const BitVector *value) {
			Base::m_value = value;
		}

	protected:
		typedef BaseIteratorImpl<IConstIteratorValue, IteratorT> Base;

		IConstIteratorImpl() = default;

		IConstIteratorImpl(const IConstIteratorImpl &) = default;
	}; // end of IConstIteratorImpl class

	template <class IteratorT>
	class INonConstIteratorImpl:
			public BaseIteratorImpl<INonConstIteratorValue, IteratorT> {
	public:
		explicit INonConstIteratorImpl(BitVector *value) {
			Base::m_value = value;
		}

		inline void SetValue(char value) {
			CHECK(value == '0' || value == '1' || value == '~',
						"Incorrect value");

			uint8_t mask = 1;
			mask <<= Base::m_current_bit_position;

			// '~' is equal to '0'
			if(value == '~') {
				value = '0';
			}

			if(value == '0') {
				mask = ~mask;
				Base::m_value->m_bytes[Base::m_current_byte_position] &= mask;
			} else {
				Base::m_value->m_bytes[Base::m_current_byte_position] |= mask;
			}
		}

	protected:
		typedef BaseIteratorImpl<INonConstIteratorValue, IteratorT> Base;

		INonConstIteratorImpl() = default;

		INonConstIteratorImpl(const INonConstIteratorImpl &) = default;
	}; // end of INonConstIteratorImpl class

	class Iterator: public INonConstIteratorImpl<Iterator> {
	public:
		explicit Iterator(BitVector *value):
				INonConstIteratorImpl(value) {
			initHighPosition();
		}

		Iterator() = default;

		Iterator(const Iterator &) = default;

		Iterator operator++() override {
			stepUp();
			return *this;
		}
	}; // end of Iterator class

	class ConstIterator: public IConstIteratorImpl<ConstIterator> {
	public:
		explicit ConstIterator(const BitVector *value):
				IConstIteratorImpl(value) {
			initHighPosition();
		}

		ConstIterator() = default;

		ConstIterator(const ConstIterator &) = default;

		ConstIterator operator++() override {
			stepUp();
			return *this;
		}
	}; // end of ConstIterator class

	class ReverseIterator: public INonConstIteratorImpl<ReverseIterator> {
	public:
		explicit ReverseIterator(BitVector *value):
				INonConstIteratorImpl(value) {
			initLowPosition();
		}

		ReverseIterator() = default;

		ReverseIterator(const ReverseIterator &) = default;

		ReverseIterator operator++() override {
			stepDown();
			return *this;
		}
	}; // end of ReverseIterator class

	class ConstReverseIterator: public IConstIteratorImpl<ConstReverseIterator> {
	public:
		explicit ConstReverseIterator(const BitVector *value):
				IConstIteratorImpl(value) {
			initLowPosition();
		}

		ConstReverseIterator() = default;

		ConstReverseIterator(const ConstReverseIterator &) = default;

		ConstReverseIterator operator++() override {
			stepDown();
			return *this;
		}
	}; // end of ConstReverseIterator

	inline Iterator begin() {
		if(*this) {
			return Iterator(this);
		}

		return Iterator();
	}

	inline ConstIterator begin() const {
		if(*this) {
			return ConstIterator(this);
		}

		return ConstIterator();
	}

	inline Iterator end() {
		return Iterator();
	}

	inline ConstIterator end() const {
		return ConstIterator();
	}

	inline ReverseIterator rbegin() {
		if(*this) {
			return ReverseIterator(this);
		}

		return ReverseIterator();
	}

	inline ConstReverseIterator rbegin() const {
		if(*this) {
			return ConstReverseIterator(this);
		}

		return ConstReverseIterator();
	}

	inline ReverseIterator rend() {
		return ReverseIterator();
	}

	inline ConstReverseIterator rend() const {
		return ConstReverseIterator();
	}

protected:
	void init(std::string_view bit_vector_by_str);

private:
	enum constants {
		BITS_PER_BYTE = 8
	};

	static inline std::size_t calculateRealByteCount(std::size_t size) {
		return (size - 1) / BITS_PER_BYTE + 1;
	}

	static inline std::size_t calculateHigherBitPositionInLastByte(std::size_t size) {
		return (size - 1) % BITS_PER_BYTE;
	}

	std::deque<uint8_t> m_bytes;

	std::size_t m_size = 0;
	std::size_t m_real_byte_count = 0;
	std::size_t m_higher_bit_position_in_last_byte = 0;
};
