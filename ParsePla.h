/**
* Created by Sergey O. Boyko on 11.11.18.
*/

#pragma once

#include "BitVector.h"
#include "TribitVector.h"
#include "PlaRoot.h"

std::optional<PlaRoot> ParsePla(const std::string &filename);